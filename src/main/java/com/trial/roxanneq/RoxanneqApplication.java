package com.trial.roxanneq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoxanneqApplication {

	public static void main(String[] args) {
		SpringApplication.run(RoxanneqApplication.class, args);
	}
}
