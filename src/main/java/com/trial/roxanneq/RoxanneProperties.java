package com.trial.roxanneq;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author: Yingyi Tang
 * @Description:
 * @Date: Create in 2018/8/20 21:56
 * @Modified by:
 */
@Component
@ConfigurationProperties(prefix = "roxanne")
public class RoxanneProperties {
    private String age;

    public String getRoxanneAge() {
        return age;
    }
}
