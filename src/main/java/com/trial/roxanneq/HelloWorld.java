package com.trial.roxanneq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Yingyi Tang
 * @Description:
 * @Date: Create in 2018/8/20 21:31
 * @Modified by:
 */
@RestController
@RequestMapping("/api/v1")
public class HelloWorld {
    @RequestMapping(value = "/say", method = RequestMethod.GET)
    public String say() {
        return "Hello Spring";
    }
    @RequestMapping(value = "/myid", method = RequestMethod.GET)
    public String sayit(@RequestParam(value = "id", required = false, defaultValue = "0") Integer myId) {
        return "My ID: " + myId;
    }

    @Value("@{test}")
    private String test;

    @Autowired
    private RoxanneProperties roxanneProperties;
    @RequestMapping(value = "/roxanne", method = RequestMethod.GET)
    public String getRoxanne() {
        return roxanneProperties.getRoxanneAge();
    }
}
